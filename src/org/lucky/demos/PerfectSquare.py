import math


def isPerfectSquare(num):
    sqr = math.sqrt(num)
    return sqr - math.floor(sqr) == 0


print(isPerfectSquare(25))
