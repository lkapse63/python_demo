num = 11


def prime(num):
    for i in range(2, num):
        if num % i == 0:
            break
    else:
        return num;


def allPrime(start, end):
    rng = range(start, end + 1)
    for i in rng:
        p = prime(i)
        if None != p:
            print(p)


allPrime(1, 100)
